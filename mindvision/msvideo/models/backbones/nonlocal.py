# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""nonlocal backbone."""

import mindspore
from mindspore import ops
from mindspore import nn
from mindspore.common.initializer import initializer, HeNormal
from mindvision.engine.class_factory import ClassFactory, ModuleType
from mindvision.msvideo.models.blocks.nonlocalblock import NonLocalBlockND
from mindvision.msvideo.ops.avgpool3d import AvgPool3D


class ResNet3DResidualBlock(nn.Cell):
    r"""
    ResNet3DResidualBlock block structure used in ResNet3D 50.
    # TODO: Resnet 3d used in r2d, i3d and every where. should be solo.

    Args:
        in_channel(int): The number of input channel.
        out_channel(int): The number of output channel.
        downsample: whether to dowmsample.

    Returns:
        Tensor, output tensor.

    Examples:
        >>> ResNet3DResidualBlock(3, 256)
    """
    expansion = 4
    def __init__(self, in_channel, out_channel, stride=1, padding=(0, 0, 1, 1, 1, 1), downsample=None):
        super(ResNet3DResidualBlock, self).__init__()
        self.conv1 = nn.Conv3d(in_channel, out_channel, kernel_size=(1, 1, 1), has_bias=False)
        self.bn1 = nn.BatchNorm3d(out_channel)
        self.conv2 = nn.Conv3d(out_channel,
                               out_channel,
                               kernel_size=(1, 3, 3),
                               stride=stride,
                               pad_mode="pad",
                               padding=padding,
                               has_bias=False)
        self.bn2 = nn.BatchNorm3d(out_channel)
        self.conv3 = nn.Conv3d(out_channel, out_channel * 4, kernel_size=(1, 1, 1), has_bias=False)
        self.bn3 = nn.BatchNorm3d(out_channel * 4)
        self.relu = nn.ReLU()
        self.downsample = downsample
        self.stride = stride

    def construct(self, x):
        """ResNet3DResidualBlock construct."""
        identity = x
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)
        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)
        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            identity = self.downsample(x)
        out += identity
        out = self.relu(out)

        return out

@ClassFactory.register(ModuleType.BACKBONE)
class ResNet3D(nn.Cell):
    """C2D with ResNet 50 backbone.
    As mentioned in Section 4. 2D ConvNet baseline (C2D),
    The only operation involving the temporal domain are the pooling layer after the second residual block.
    For more details of the structure, refer to Table 1 from the paper.
    Padding was added accordingly to match the correct dimensionality.

    Xiaolong Wang, Ross Girshick, Abhinav Gupta, Kaiming He (CMU, FAIR).
    "Non-local Neural Networks."
    https://arxiv.org/abs/1711.07971v1

    Args:
        layers (list): Numbers of block in different layers.
        block (Cell): Block for network.
        num_classes (int): The number of classes that the training images are belonging to.
        non_local: whether to add nonlocal block.

    Returns:
        Tensor, output tensor.

    Examples:
        >>> ResNet3D([3, 4, 6, 3], non_local=True)
    """

    def __init__(self, layers, block=ResNet3DResidualBlock, num_classes=400, non_local=False):
        self.in_channel = 64
        self.num_classes = num_classes
        super(ResNet3D, self).__init__()

        self.conv1 = nn.Conv3d(3,
                               64,
                               kernel_size=(1, 7, 7),
                               stride=2,
                               pad_mode="pad",
                               padding=(0, 0, 3, 3, 3, 3),
                               has_bias=False)
        self.bn1 = nn.BatchNorm3d(64)
        self.relu = nn.ReLU()
        self.pool1 = ops.MaxPool3D(kernel_size=3, strides=2)
        self.layer1 = self._make_layer(block,
                                       64,
                                       layers[0],
                                       stride=1,
                                       padding=(0, 0, 1, 1, 1, 1),
                                       d_padding=0)
        self.pool_t = ops.MaxPool3D(kernel_size=(3, 1, 1), strides=(2, 1, 1))
        self.layer2 = self._make_layer(block,
                                       128,
                                       layers[1],
                                       stride=2,
                                       padding=(2, 2, 1, 1, 1, 1),
                                       d_padding=(2, 2, 0, 0, 0, 0))
        self.layer3 = self._make_layer(block,
                                       256,
                                       layers[2],
                                       stride=2,
                                       padding=(2, 2, 1, 1, 1, 1),
                                       d_padding=(2, 2, 0, 0, 0, 0),
                                       non_local=non_local)
        self.layer4 = self._make_layer(block,
                                       512,
                                       layers[3],
                                       stride=2,
                                       padding=(2, 2, 1, 1, 1, 1),
                                       d_padding=(2, 2, 0, 0, 0, 0))
        self.avgpool = AvgPool3D(kernel_size=(4, 7, 7))

        for m in self.cells_and_names():
            if isinstance(m, nn.Conv3d):
                m.weight = initializer(HeNormal(mode='fan_out'), m.weight.shape, mindspore.float32)
            elif isinstance(m, nn.BatchNorm3d):
                fill = ops.Fill()
                zeroslike = ops.ZerosLike()
                fill(mindspore.float32, m.bn2d.gamma.data, 1)
                zeroslike(m.bn2d.beta.data)

    def _make_layer(self, block, out_channel, blocks, stride=1, padding=0, d_padding=0, non_local=False):
        """
        Make stage network of ResNet3D.

        Args:
            block (Cell): ResNet3D block.
            out_channel (int): Output channel.
            blocks(int): Layer number.
            stride (int): Stride size for the first convolutional layer.
            non_local: whether to add nonlocal block.

        Returns:
            SequentialCell, the output layer.

        Examples:
            >>> _make_layer(ResNet3DResidualBlock, 3, 128, 256, 2)
        """
        downsample = nn.SequentialCell(
            nn.Conv3d(self.in_channel,
                      out_channel * block.expansion,
                      kernel_size=1,
                      stride=stride,
                      pad_mode="pad",
                      padding=d_padding,
                      has_bias=False),
            nn.BatchNorm3d(out_channel * block.expansion)
                        )

        layers = []
        layers.append(block(self.in_channel, out_channel, stride, padding, downsample))
        self.in_channel = out_channel * block.expansion
        last_idx = blocks
        if non_local:
            last_idx = blocks - 1
        for _ in range(1, last_idx):
            layers.append(block(self.in_channel, out_channel))

        # add non-local block here
        if non_local:
            layers.append(NonLocalBlockND(in_channels=1024))
            layers.append(block(self.in_channel, out_channel))

        return nn.SequentialCell(*layers)

    def construct(self, x):
        """ResNet3D construct."""
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.pool1(x)
        x = self.layer1(x)
        x = self.pool_t(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        x = self.avgpool(x)
        x = x.view(x.shape[0], -1)

        return x
