# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Video models."""

import math

import mindspore.nn as nn

import mindvision.msvideo.utils.init_weight as init_helper
from mindvision.classification.models.utils import make_divisible
from mindvision.msvideo.models.blocks import x3d_resnet as resnet_helper
from mindvision.msvideo.models.blocks import x3d_stem as stem_helper
from mindvision.engine.class_factory import ClassFactory, ModuleType

# Number of blocks for different stages given the model depth.
_MODEL_STAGE_DEPTH = {50: (3, 4, 6, 3), 101: (3, 4, 23, 3)}

# Basis of temporal kernel sizes for each of the stage.
_TEMPORAL_KERNEL_BASIS = {
    "x3d": [
        [[5]],  # conv1 temporal kernels.
        [[3]],  # res2 temporal kernels.
        [[3]],  # res3 temporal kernels.
        [[3]],  # res4 temporal kernels.
        [[3]],  # res5 temporal kernels.
    ],
}

_POOL1 = {
    "x3d": [[1, 1, 1]],
}


@ClassFactory.register(ModuleType.BACKBONE)
class X3D(nn.Cell):
    """
    X3D model builder. It builds a X3D network backbone, which is a ResNet.

    Christoph Feichtenhofer.
    "X3D: Expanding Architectures for Efficient Video Recognition."
    https://arxiv.org/abs/2004.04730

    Args:
        width_factor(float): Width expansion factor.
        depth_factor(float): Depth expansion factor.
        bottleneck_factor(float): Bottleneck expansion factor for the 3x3x3 conv.
        dim_c1(int): Dimensions of the first 3x3 conv layer.
        fc_init_std(float): The std to initialize the fc layer(s).
        scale_res2(bool): Whether to scale the width of Res2, default is false.
        zero_init_final_bn(bool): If true, initialize the gamma of the
            final BN of each block to zero.
        input_channel_num(list): List of input frame channel dimensions.

    Returns:
        Tensor

    Examples:
        >>> backbone = X3D(width_factor=2.0, depth_factor=2.2, bottleneck_factor=2.25,
        >>>             dim_c1=12, fc_init_std=0.01, scale_res2=False,
        >>>             zero_init_final_bn=False, input_channel_num=[3])

    """

    def __init__(self,
                 width_factor=2.0,
                 depth_factor=2.2,
                 bottleneck_factor=2.25,
                 dim_c1=12,
                 fc_init_std=0.01,
                 scale_res2=False,
                 zero_init_final_bn=False,
                 input_channel_num=None
                 ):
        super(X3D, self).__init__()
        self.norm_module = nn.BatchNorm3d
        # Whether enable video detection.
        self.enable_detection = False
        self.num_pathways = 1

        exp_stage = 2.0
        self.dim_c1 = dim_c1

        self.dim_res2 = (
            make_divisible(self.dim_c1 * exp_stage, divisor=8)
            if scale_res2
            else self.dim_c1
        )
        self.dim_res3 = make_divisible(self.dim_res2 * exp_stage, divisor=8)
        self.dim_res4 = make_divisible(self.dim_res3 * exp_stage, divisor=8)
        self.dim_res5 = make_divisible(self.dim_res4 * exp_stage, divisor=8)

        self.width_factor = width_factor
        self.depth_factor = depth_factor
        self.bottleneck_factor = bottleneck_factor
        self.input_channel_num = input_channel_num

        self.block_basis = [
            # blocks, c, stride
            [1, self.dim_res2, 2],
            [2, self.dim_res3, 2],
            [5, self.dim_res4, 2],
            [3, self.dim_res5, 2],
        ]
        self.seq = nn.SequentialCell()
        self._construct_network()
        # init_helper
        init_helper.init_weights(
            self, fc_init_std, zero_init_final_bn
        )

    def _round_repeats(self, repeats, multiplier):
        """Round number of layers based on depth multiplier."""
        multiplier = multiplier
        if not multiplier:
            return repeats
        return int(math.ceil(multiplier * repeats))

    def _construct_network(self):
        """
        Builds a single pathway X3D model.
        """

        w_mul = self.width_factor
        d_mul = self.depth_factor
        dim_res1 = make_divisible(self.dim_c1 * w_mul, divisor=8)

        temp_kernel = _TEMPORAL_KERNEL_BASIS['x3d']

        s1 = stem_helper.VideoModelStem(
            dim_in=self.input_channel_num,
            dim_out=[dim_res1],
            kernel=[temp_kernel[0][0] + [3, 3]],
            stride=[[1, 2, 2]],
            padding=[[temp_kernel[0][0][0] // 2, 1, 1]],
            norm_module=self.norm_module
        )
        self.seq.append(s1)

        dim_in = dim_res1

        for stage, block in enumerate(self.block_basis):
            dim_out = make_divisible(block[1] * w_mul, divisor=8)
            dim_inner = int(self.bottleneck_factor * dim_out)
            n_rep = self._round_repeats(block[0], d_mul)

            s = resnet_helper.ResStage(
                dim_in=[dim_in],
                dim_out=[dim_out],
                dim_inner=[dim_inner],
                temp_kernel_sizes=temp_kernel[1],
                stride=[block[2]],
                num_blocks=[n_rep],
                # num_groups=[dim_inner],
                num_block_temp_kernel=[n_rep],
                stride_1x1=False,
                norm_module=self.norm_module,
                dilation=[1, 1],
                drop_connect_rate=0.5 * (stage + 2) / (len(self.block_basis) + 1),
            )
            dim_in = dim_out
            self.seq.append(s)

    def construct(self, x):
        x = self.seq(x)
        return x
