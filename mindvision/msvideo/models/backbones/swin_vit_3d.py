# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Swin Transformer 3D Backbone."""

import numpy as np

from mindspore import nn

from mindvision.engine.class_factory import ClassFactory
from mindvision.engine.class_factory import ModuleType
from mindvision.msvideo.models.blocks import Identity
from mindvision.msvideo.models.blocks import PatchEmbed3D
from mindvision.msvideo.models.blocks import PatchMerging
from mindvision.msvideo.models.blocks import SwinTransformerStage3D


@ClassFactory.register(ModuleType.BACKBONE)
class SwinTransformer3D(nn.Cell):
    """
    Video Swin Transformer backbone.
    A mindspore implementation of : `Video Swin Transformer`  -
        http://arxiv.org/abs/2106.13230

    Args:
        patch_size (int | tuple(int)): Patch size. Default: (4,4,4).
        in_chans (int): Number of input image channels. Default: 3.
        embed_dim (int): Number of linear projection output channels.
            Default: 96.
        depths (tuple[int]): Depths of each Swin Transformer stage.
        num_heads (tuple[int]): Number of attention head of each stage.
        window_size (int): Window size. Default: 7.
        mlp_ratio (float): Ratio of mlp hidden dim to embedding dim.
            Default: 4.
        qkv_bias (bool): If True, add a learnable bias to query, key, value.
            Default: Truee
        qk_scale (float): Override default qk scale of head_dim ** -0.5 if set.
        keep_prob (float): Dropout keep probability.
        attn_keep_prob (float): Attention dropout keep probability. Default: 1.
        droppath_keep_prob (float): Stochastic depth keep probability.
            Default: 0.8.
        norm_layer: Normalization layer. Default: nn.LayerNorm.
        patch_norm (bool): If True, add normalization after patch embedding.
            Default: True.
    """

    def __init__(self,
                 pretrained=None,
                 pretrained2d=True,
                 input_size=(16, 224, 224),
                 patch_size=(4, 4, 4),
                 in_channels=3,
                 embed_dim=96,
                 depths=(2, 2, 6, 2),
                 num_heads=(3, 6, 12, 24),
                 window_size=(8, 7, 7),
                 mlp_ratio=4.,
                 qkv_bias=True,
                 qk_scale=None,
                 keep_prob=1.,
                 attn_keep_prob=1.,
                 droppath_keep_prob=0.8,
                 norm_layer='layernorm',
                 patch_norm=True
                 ):
        super(SwinTransformer3D, self).__init__()

        self.pretrained = pretrained
        self.pretrained2d = pretrained2d
        self.num_layers = len(depths)
        self.embed_dim = embed_dim
        self.patch_norm = patch_norm
        self.window_size = window_size
        self.patch_size = patch_size
        # split image into non-overlapping patches
        self.patch_embed = PatchEmbed3D(
            input_size=input_size, patch_size=patch_size,
            in_channels=in_channels, embed_dim=embed_dim,
            norm_layer=norm_layer if self.patch_norm else None)
        self.patch_size = self.patch_embed.output_size
        self.pos_drop = nn.Dropout(keep_prob=keep_prob)

        # stochastic depth decay rule
        dpr = list(np.linspace(1, droppath_keep_prob, sum(depths)))

        # build layers
        self.layers = nn.CellList()
        for i_layer in range(self.num_layers):
            layer = SwinTransformerStage3D(
                dim=int(embed_dim * 2**i_layer),
                input_size=(self.patch_size[0],
                            self.patch_size[1] // (2 ** i_layer),
                            self.patch_size[2] // (2 ** i_layer)),
                depth=depths[i_layer],
                num_heads=num_heads[i_layer],
                window_size=window_size,
                mlp_ratio=mlp_ratio,
                qkv_bias=qkv_bias,
                qk_scale=qk_scale,
                keep_prob=keep_prob,
                attn_keep_prob=attn_keep_prob,
                droppath_keep_prob=dpr[sum(
                    depths[:i_layer]):sum(depths[:i_layer + 1])],
                norm_layer=norm_layer,
                downsample=PatchMerging if i_layer < self.num_layers - 1
                else None
                )
            self.layers.append(layer)

        self.num_features = int(embed_dim * 2**(self.num_layers-1))

        # add a norm layer for each output
        if norm_layer == 'layernorm':
            self.norm = nn.LayerNorm((self.num_features,))
        else:
            self.norm = Identity()

    def construct(self, x):
        x = self.patch_embed(x)
        x = self.pos_drop(x)
        for layer in self.layers:
            x = layer(x)
        x = self.norm(x)
        x = x.transpose(0, 4, 1, 2, 3)
        return x
