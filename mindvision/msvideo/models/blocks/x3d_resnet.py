# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Video models."""

import mindspore.nn as nn

from mindvision.msvideo.utils.others import drop_path
from mindvision.msvideo.models.blocks.squeeze_excite3d import SqueezeExcite3D
from mindvision.classification.engine.ops.swish import Swish


class X3DTransform(nn.Cell):
    """
    X3D transformation: 1x1x1, Tx3x3 (channelwise, num_groups=dim_in), 1x1x1,
        augmented with (optional) SE (squeeze-excitation) on the 3x3x3 output.
        T is the temporal kernel size (defaulting to 3)

    Args:
        dim_in (int): the channel dimensions of the input.
        dim_out (int): the channel dimension of the output.
        temp_kernel_size (int): the temporal kernel sizes of the middle
            convolution in the bottleneck.
        stride (int): the stride of the bottleneck.
        dim_inner (int): the inner dimension of the block.
        num_groups (int): number of groups for the convolution. num_groups=1
            is for standard ResNet like networks, and num_groups>1 is for
            ResNeXt like networks.
        stride_1x1 (bool): if True, apply stride to 1x1 conv, otherwise
            apply stride to the 3x3 conv.
        inplace_relu (bool): if True, calculate the relu on the original
            input without allocating new memory.
        eps (float): epsilon for batch norm.
        bn_mmt (float): momentum for batch norm. Noted that BN momentum in
            PyTorch = 1 - BN momentum in Caffe2.
        dilation (int): size of dilation.
        norm_module (nn.Module): nn.Module for the normalization layer. The
            default is nn.BatchNorm3d.
        se_ratio (float): if > 0, apply SE to the Tx3x3 conv, with the SE
            channel dimensionality being se_ratio times the Tx3x3 conv dim.
        swish_inner (bool): if True, apply swish to the Tx3x3 conv, otherwise
            apply ReLU to the Tx3x3 conv.

    Returns:
        Tensor

    Examples:
        >>> X3DTransform(dim_in=24, dim_out=24, temp_kernel_size=3, stride=2,
        >>>             dim_inner=54, stride_1x1=False, inplace_relu=True,
        >>>             dilation=1, norm_module=nn.BatchNorm3d, block_idx=1)

    """

    def __init__(self,
                 dim_in,
                 dim_out,
                 temp_kernel_size,
                 stride,
                 dim_inner,
                 # num_groups,
                 stride_1x1=False,
                 inplace_relu=True,
                 eps=1e-5,
                 bn_mmt=0.1,
                 dilation=1,
                 norm_module=nn.BatchNorm3d,
                 se_ratio=0.0625,
                 swish_inner=True,
                 block_idx=0,
                 ):
        super(X3DTransform, self).__init__()
        self.temp_kernel_size = temp_kernel_size
        self._inplace_relu = inplace_relu
        self._eps = eps
        self._bn_mmt = bn_mmt
        self._se_ratio = se_ratio
        self._swish_inner = swish_inner
        self._stride_1x1 = stride_1x1
        self._block_idx = block_idx
        self.se = None
        (str1x1, str3x3) = (stride, 1) if self._stride_1x1 else (1, stride)
        self.a = nn.Conv3d(
            dim_in,
            dim_inner,
            kernel_size=(1, 1, 1),
            stride=(1, str1x1, str1x1),
            padding=0,
            has_bias=False,
        )
        self.a_bn = norm_module(
            num_features=dim_inner, eps=self._eps, momentum=self._bn_mmt
        )
        self.a_relu = nn.ReLU()
        self.b = nn.Conv3d(
            dim_inner,
            dim_inner,
            (self.temp_kernel_size, 3, 3),
            stride=(1, str3x3, str3x3),
            pad_mode='pad',
            padding=(
                int(self.temp_kernel_size // 2),
                int(self.temp_kernel_size // 2),
                dilation,
                dilation,
                dilation,
                dilation),
            # NOTE:for now group only support 1
            # group=num_groups,
            group=1,
            has_bias=False,
            dilation=(1, dilation, dilation),
        )
        self.b_bn = norm_module(
            num_features=dim_inner, eps=self._eps, momentum=self._bn_mmt
        )

        # Apply SE attention or not
        use_se = False
        if (self._block_idx + 1) % 2:
            use_se = True
        if self._se_ratio > 0.0 and use_se:
            self.se = SqueezeExcite3D(dim_inner, self._se_ratio)
        if self._swish_inner:
            self.b_relu = Swish()
        else:
            self.b_relu = nn.ReLU()

        # 1x1x1, BN.
        self.c = nn.Conv3d(
            dim_inner,
            dim_out,
            kernel_size=(1, 1, 1),
            stride=(1, 1, 1),
            padding=0,
            has_bias=False,
        )
        self.c_bn = norm_module(
            num_features=dim_out, eps=self._eps, momentum=self._bn_mmt
        )
        self.c_bn.transform_final_bn = True

    def construct(self, x):
        """ construct X3DTransform """
        x = self.a(x)
        x = self.a_bn(x)
        x = self.a_relu(x)
        x = self.b(x)
        x = self.b_bn(x)
        if self.se:
            x = self.se(x)
        x = self.b_relu(x)
        x = self.c(x)
        x = self.c_bn(x)
        return x


class ResBlock(nn.Cell):
    """
    Residual block.

    ResBlock class constructs redisual blocks. More details can be found in:
            Kaiming He, Xiangyu Zhang, Shaoqing Ren, and Jian Sun.
            "Deep residual learning for image recognition."
            https://arxiv.org/abs/1512.03385

    Args:
        dim_in (int): the channel dimensions of the input.
        dim_out (int): the channel dimension of the output.
        temp_kernel_size (int): the temporal kernel sizes of the middle
            convolution in the bottleneck.
        stride (int): the stride of the bottleneck.
        dim_inner (int): the inner dimension of the block.
        num_groups (int): number of groups for the convolution. num_groups=1
            is for standard ResNet like networks, and num_groups>1 is for
            ResNeXt like networks.
        stride_1x1 (bool): if True, apply stride to 1x1 conv, otherwise
            apply stride to the 3x3 conv.
        inplace_relu (bool): calculate the relu on the original input
            without allocating new memory.
        eps (float): epsilon for batch norm.
        bn_mmt (float): momentum for batch norm. Noted that BN momentum in
            PyTorch = 1 - BN momentum in Caffe2.
        dilation (int): size of dilation.
        norm_module (nn.Module): nn.Module for the normalization layer. The
            default is nn.BatchNorm3d.
        drop_connect_rate (float): basic rate at which blocks are dropped,
            linearly increases from input to output blocks.

    Returns:
        Tensor

    Examples:
        >>> ResBlock(dim_in=[24], dim_out=[24], temp_kernel_size=[3, 3, 3], stride=[2],
        >>>         dim_inner=[54], stride_1x1=False, inplace_relu=True, dilation=[1, 1],
        >>>         norm_module=nn.BatchNorm3d, block_idx=[1], drop_connect_rate=0.2)

    """

    def __init__(self,
                 dim_in,
                 dim_out,
                 temp_kernel_size,
                 stride,
                 dim_inner,
                 stride_1x1=False,
                 inplace_relu=True,
                 eps=1e-5,
                 bn_mmt=0.1,
                 dilation=1,
                 norm_module=nn.BatchNorm3d,
                 block_idx=0,
                 drop_connect_rate=0.0,
                 ):
        super(ResBlock, self).__init__()
        self._inplace_relu = inplace_relu
        self._eps = eps
        self._bn_mmt = bn_mmt
        self._drop_connect_rate = drop_connect_rate
        self.branch1 = None
        self._construct(
            dim_in,
            dim_out,
            temp_kernel_size,
            stride,
            dim_inner,
            # num_groups,
            stride_1x1,
            inplace_relu,
            dilation,
            norm_module,
            block_idx,
        )

    def _construct(
            self,
            dim_in,
            dim_out,
            temp_kernel_size,
            stride,
            dim_inner,
            stride_1x1,
            inplace_relu,
            dilation,
            norm_module,
            block_idx,
    ):
        """ construct ResBlock """
        # Use skip connection with projection if dim or res change.
        if (dim_in != dim_out) or (stride != 1):
            self.branch1 = nn.Conv3d(
                dim_in,
                dim_out,
                kernel_size=1,
                stride=(1, stride, stride),
                padding=0,
                has_bias=False,
                dilation=1,
            )
            self.branch1_bn = norm_module(
                num_features=dim_out, eps=self._eps, momentum=self._bn_mmt
            )
        self.branch2 = X3DTransform(
            dim_in,
            dim_out,
            temp_kernel_size,
            stride,
            dim_inner,
            # num_groups,
            stride_1x1=stride_1x1,
            inplace_relu=inplace_relu,
            dilation=dilation,
            norm_module=norm_module,
            block_idx=block_idx,
        )
        self.relu = nn.ReLU()

    def construct(self, x):
        """ build ResBlock """
        f_x = self.branch2(x)
        if self.training and self._drop_connect_rate > 0.0:
            f_x = drop_path(f_x, self._drop_connect_rate)

        if self.branch1:
            x = self.branch1_bn(self.branch1(x)) + f_x
        else:
            x = x + f_x
        x = self.relu(x)
        return x


class ResStage(nn.Cell):
    """
    Stage of 3D ResNet. It expects to have one or more tensors as input for
        single pathway (C2D, I3D, Slow), and multi-pathway (SlowFast) cases.
        More details can be found here:

        Christoph Feichtenhofer, Haoqi Fan, Jitendra Malik, and Kaiming He.
        "SlowFast networks for video recognition."
        https://arxiv.org/pdf/1812.03982.pdf

    Args:
        dim_in (list): list of p the channel dimensions of the input.
            Different channel dimensions control the input dimension of
            different pathways.
        dim_out (list): list of p the channel dimensions of the output.
            Different channel dimensions control the input dimension of
            different pathways.
        temp_kernel_sizes (list): list of the p temporal kernel sizes of the
            convolution in the bottleneck. Different temp_kernel_sizes
            control different pathway.
        stride (list): list of the p strides of the bottleneck. Different
            stride control different pathway.
        num_blocks (list): list of p numbers of blocks for each of the
            pathway.
        dim_inner (list): list of the p inner channel dimensions of the
            input. Different channel dimensions control the input dimension
            of different pathways.
        num_groups (list): list of number of p groups for the convolution.
            num_groups=1 is for standard ResNet like networks, and
            num_groups>1 is for ResNeXt like networks.
        num_block_temp_kernel (list): extent the temp_kernel_sizes to
            num_block_temp_kernel blocks, then fill temporal kernel size
            of 1 for the rest of the layers.
        dilation (list): size of dilation for each pathway.
        norm_module (nn.Module): nn.Module for the normalization layer. The
            default is nn.BatchNorm3d.
        drop_connect_rate (float): basic rate at which blocks are dropped,
            linearly increases from input to output blocks.

    Returns:
        Tensor

    Examples:
        >>> ResStage(dim_in=[24], dim_out=[24], dim_inner=[54], stride=[2],
        >>>         temp_kernel_sizes=[3], num_blocks=[3], num_block_temp_kernel=[3],
        >>>         dilation=[1, 1], stride_1x1=False, inplace_relu=True,
        >>>         norm_module=nn.BatchNorm3d, drop_connect_rate=0.2)

    """

    def __init__(self,
                 dim_in,
                 dim_out,
                 stride,
                 temp_kernel_sizes,
                 num_blocks,
                 dim_inner,
                 # num_groups,
                 num_block_temp_kernel,
                 dilation,
                 stride_1x1=False,
                 inplace_relu=True,
                 norm_module=nn.BatchNorm3d,
                 drop_connect_rate=0.0,
                 ):
        super(ResStage, self).__init__()
        self.num_blocks = num_blocks
        self._drop_connect_rate = drop_connect_rate
        self.temp_kernel_sizes = [
            (temp_kernel_sizes[i] * num_blocks[i])[: num_block_temp_kernel[i]]
            + [1] * (num_blocks[i] - num_block_temp_kernel[i])
            for i in range(len(temp_kernel_sizes))
        ]
        self.num_pathways = len(self.num_blocks)
        self.seq = nn.SequentialCell()
        # x3d only have one pathway
        for pathway in range(self.num_pathways):
            for i in range(self.num_blocks[pathway]):
                # Construct the block.
                res_block = ResBlock(
                    dim_in[pathway] if i == 0 else dim_out[pathway],
                    dim_out[pathway],
                    self.temp_kernel_sizes[pathway][i],
                    stride[pathway] if i == 0 else 1,
                    dim_inner[pathway],
                    # num_groups[pathway],
                    stride_1x1=stride_1x1,
                    inplace_relu=inplace_relu,
                    dilation=dilation[pathway],
                    norm_module=norm_module,
                    block_idx=i,
                    drop_connect_rate=self._drop_connect_rate,
                )
                self.seq.append(res_block)

    def construct(self, x):
        x = self.seq(x)
        return x
