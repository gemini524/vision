# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
""" SpatialAttention for ARN."""

import mindspore.nn as nn

from mindvision.msvideo.engine.ops import MaxPool3D


class SpatialAttention(nn.Cell):
    """
    Initialize spatial attention unit which refine the aggregation step
    by re-weighting block contributions.

    Args:
        in_channels: The number of channels of the input feature.
        out_channels: The number of channels of the output of hidden layers.

    Returns:
        Tensor of shape (1, 1, H, W).
    """

    def __init__(self, in_channels, out_channels):
        super(SpatialAttention, self).__init__()
        self.sp_att = nn.SequentialCell(
            nn.Conv3d(in_channels, out_channels, kernel_size=3, pad_mode='pad', padding=1),
            nn.BatchNorm3d(out_channels, momentum=1, affine=True),
            nn.ReLU(),
            MaxPool3D((2, 1, 1)),  # frame/2 x 32 x 32 x 32
            nn.Conv3d(out_channels, out_channels, kernel_size=3, pad_mode='pad', padding=1),
            nn.BatchNorm3d(out_channels, momentum=1, affine=True),
            nn.ReLU(),
            MaxPool3D((2, 1, 1)),
            nn.Conv3d(out_channels, 1, kernel_size=1, padding=0),
            nn.Sigmoid())  # 1 x 1 x H x W

    def construct(self, x):
        out = self.sp_att(x)
        return out
