# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""Swin Transformer 3D block."""

from typing import Optional

from mindspore import nn
from mindspore import ops

from mindvision.check_param import Rel
from mindvision.check_param import Validator
from mindvision.classification.models.blocks.feed_forward import FeedForward
from mindvision.msvideo.models.blocks import FixedprobDropPath1D
from mindvision.msvideo.models.blocks import Identity
from mindvision.msvideo.models.blocks import WindowAttention3D
from mindvision.msvideo.utils import limit_window_size
from mindvision.msvideo.utils import window_partition
from mindvision.msvideo.utils import window_reverse
from mindvision.msvideo.utils import compute_mask
from mindvision.msvideo.engine.ops import Roll3D


class SwinTransformerBlock3D(nn.Cell):
    """
    A Video Swin Transformer Block. The implementation of this block follows
    the paper
    "Video Swin Transformer".

    Args:
        dim (int): Number of input channels.
        num_heads (int): Number of attention heads.
        window_size (tuple[int]): Window size.
        shift_size (tuple[int]): Shift size for SW-MSA.
        mlp_ratio (float): Ratio of mlp hidden dim to embedding dim.
        qkv_bias (bool): If True, add a learnable bias to query, key,value.
            Default: True.
        qk_scale (float | None, optional): Override default qk scale of
            head_dim ** -0.5 if set True.
        keep_prob (float): Dropout keep rate. Default: 1.0.
        attn_keep_prob (float): Attention dropout keep rate. Default: 1.0.
        dropout_path (float): Stochastic dropout keep rate. Default: 1.0.
        act_layer (nn.Cell): Activation layer. Default: nn.GELU.
        norm_layer (nn.Cell): Normalization layer. Default: nn.LayerNorm.

    Inputs:
        - **x** (Tensor) - Input feature of shape (B, D, H, W, C).
        - **mask_matrix** (Tensor) - Attention mask for cyclic shift.

    Outputs:
        Tensor of shape (B, D, H, W, C)

    Examples:
        >>> net1 = SwinTransformerBlock3D(96, 3, [16, 112, 112], [8, 7, 7],
            [0,0,0], 4, True, None, 0., 0., 0., nn.GELU, nn.LayerNorm)
        >>> input = ops.Zeros()((1,16,112,112,96), mindspore.float32)
        >>> output = net1(input, None)
        >>> print(output.shape)
    """

    def __init__(self,
                 dim: int,
                 num_heads: int,
                 input_size: int = (0, 0, 0),
                 window_size: int = (2, 7, 7),
                 shift_size: int = (0, 0, 0),
                 mlp_ratio: float = 4.,
                 qkv_bias: bool = True,
                 qk_scale: Optional[float] = None,
                 keep_prob: float = 1.,
                 attn_keep_prob: float = 1.,
                 droppath_keep_prob: float = 1.,
                 act_layer: nn.Cell = nn.GELU,
                 norm_layer: str = 'layernorm'
                 ):
        super().__init__()
        self.dim = dim
        self.num_heads = num_heads
        self.window_size = window_size
        self.shift_size = shift_size
        self.mlp_ratio = mlp_ratio
        # get window size and shift size
        self.window_size, self.shift_size = limit_window_size(
            input_size, window_size, shift_size)
        # check self.shift_size whether is smaller than self.window_size and larger than 0
        Validator.check_int_range(
            self.shift_size[0], 0, self.window_size[0],
            Rel.INC_LEFT,
            arg_name="shift size", prim_name="SwinTransformerBlock3D")
        Validator.check_int_range(
            self.shift_size[1], 0, self.window_size[1],
            Rel.INC_LEFT, arg_name="shift size",
            prim_name="SwinTransformerBlock3D")
        Validator.check_int_range(
            self.shift_size[2], 0, self.window_size[2],
            Rel.INC_LEFT, arg_name="shift size",
            prim_name="SwinTransformerBlock3D")
        if isinstance(self.dim, int):
            self.dim = (self.dim,)
        # the first layer norm
        if norm_layer == 'layernorm':
            self.norm1 = nn.LayerNorm(self.dim, epsilon=1e-5)
        else:
            self.norm1 = Identity()
        # window attention 3D block
        self.attn = WindowAttention3D(
            self.dim[0],
            window_size=self.window_size,
            num_heads=num_heads,
            qkv_bias=qkv_bias,
            qk_scale=qk_scale,
            attn_kepp_prob=attn_keep_prob,
            proj_keep_prob=keep_prob
        )
        self.drop_path = FixedprobDropPath1D(
            droppath_keep_prob) if droppath_keep_prob < 1. else Identity()
        # the second layer norm
        if norm_layer == 'layernorm':
            self.norm2 = nn.LayerNorm(self.dim, epsilon=1e-5)
        else:
            self.norm2 = Identity()
        mlp_hidden_dim = int(self.dim[0] * mlp_ratio)
        # reuse classification.models.block.feed_forward as a MLP(multi layer perceptron) here
        self.mlp = FeedForward(
            in_features=self.dim[0],
            hidden_features=mlp_hidden_dim,
            activation=act_layer,
            keep_prob=keep_prob
        )

    def _construc_part1(self, x, mask_matrix):
        """"Construct W-MSA and SW-MSA."""
        batch_size, depth, height, width, channel_num = x.shape
        window_size = self.window_size
        shift_size = self.shift_size
        x = self.norm1(x)
        # pad feature maps to multiples of window size
        pad_l = pad_t = pad_d0 = 0
        pad_d1 = (window_size[0] - depth % window_size[0]) % window_size[0]
        pad_b = (window_size[1] - height % window_size[1]) % window_size[1]
        pad_r = (window_size[2] - width % window_size[2]) % window_size[2]
        x_padded = []
        pad = nn.Pad(paddings=(
            (pad_d0, pad_d1),
            (pad_t, pad_b),
            (pad_l, pad_r),
            (0, 0)))
        for i in range(x.shape[0]):
            x_b = x[i]
            x_b = pad(x_b)
            x_padded.append(x_b)
        x = ops.Stack(axis=0)(x_padded)
        # cyclic shift
        _, t_padded, h_padded, w_padded, _ = x.shape
        if [i for i in shift_size if i > 0]:
            shifted_x = Roll3D(shift=[-i for i in shift_size])(x)
            attn_mask = mask_matrix
        else:
            shifted_x = x
            attn_mask = None
        # partition windows: (B*nW, Wd*Wh*Ww, C)
        x_windows = window_partition(shifted_x, window_size)
        # W-MSA/SW-MSA
        # attn_windows: (B*nW, Wd*Wh*Ww, C)
        attn_windows = self.attn(x_windows, mask=attn_mask)
        # merge windows
        attn_windows = attn_windows.view(-1, *(window_size+(channel_num,)))
        shifted_x = window_reverse(attn_windows, window_size, batch_size,
                                   t_padded, h_padded, w_padded)
        # reverse cyclic shift
        if [i for i in shift_size if i > 0]:
            x = Roll3D(shift=shift_size)(x)
        else:
            x = shifted_x
        if pad_d1 > 0 or pad_r > 0 or pad_b > 0:
            x = x[:, :depth, :height, :width, :]
        return x

    def _construct_part2(self, x):
        """Construct MLP."""
        x = self.norm2(x)
        x = self.mlp(x)
        x = self.drop_path(x)
        return x

    def construct(self, x, mask_matrix=None):
        """Construct 3D Swin Transformer Block."""
        shortcut = x
        x = self._construc_part1(x, mask_matrix)
        x = shortcut + self.drop_path(x)
        x = x + self._construct_part2(x)
        return x


class PatchMerging(nn.Cell):
    """
    Patch Merging Layer.

    Args:
        dim (int): Number of input channels.
        norm_layer (nn.Cell): Normalization layer. Default: nn.LayerNorm。

    Inputs:
        - **x** (Tensor) - Input feature of shape (B, D, H, W, C).

    Outputs:
        Tensor of shape (B, D, H/2, W/2, 2*C)
    """

    def __init__(self,
                 dim: int = 96,
                 norm_layer: str = 'layernorm'):
        super().__init__()
        self.dim = dim
        self.reduction = nn.Dense(4 * self.dim, 2 * self.dim, has_bias=False)
        if norm_layer == 'layernorm':
            self.norm = nn.LayerNorm((4 * self.dim,))
        else:
            self.norm = Identity()

    def construct(self, x):
        """Construct Patch Merging Layer."""

        batch_size, _, height, width, _ = x.shape

        # padding
        pad_input = (height % 2 == 1) or (width % 2 == 1)
        if pad_input:
            x_padded = []
            pad = nn.Pad(paddings=((0, 0), (0, height % 2), (0, width % 2), (0, 0)))
            for i in range(batch_size):
                x_b = x[i]
                x_b = pad(x_b)
                x_padded.append(x_b)
            x_padded = ops.Stack(axis=0)(x_padded)
            x = x_padded
        # x_0, x_1, x_2, x_3: (B, D, H/2, W/2, C)
        x_0 = x[:, :, 0::2, 0::2, :]
        x_1 = x[:, :, 1::2, 0::2, :]
        x_2 = x[:, :, 0::2, 1::2, :]
        x_3 = x[:, :, 1::2, 1::2, :]
        # x: (B, D, H/2, W/2, 4*C)
        x = ops.Concat(axis=-1)([x_0, x_1, x_2, x_3])

        x = self.norm(x)
        x = self.reduction(x)

        return x


class SwinTransformerStage3D(nn.Cell):
    r"""
    A basic Swin Transformer layer for one stage.

    Args:
        dim (int): Number of feature channels.
        input_size (tuple[int]): Input video size. E.g. (16, 56, 56).
        depth (int): Depths of this stage.
        num_heads (int): Number of attention head.
        window_size (tuple[int]): Local window size. Default: (1,7,7).
        mlp_ratio (float): Ratio of mlp hidden dim to embedding dim.
            Default: 4.
        qkv_bias (bool, optional): If True, add a learnable bias to query,
            key, value. Default: True
        qk_scale (float | None, optional): Override default qk scale of
            head_dim ** -0.5 if set.
        keep_prob (float): Dropout keep rate. Default: 1.0.
        attn_keep_prob (float): Attention dropout keep rate. Default: 1.0.
        droppath_keep_prob (float | Tuple[float]): Stochastic dropout keep
            rate. Default: 1.0.
        norm_layer (nn.Module, optional): Normalization layer. Default:
            nn.LayerNorm
        downsample (nn.Module | None, optional): Downsample layer at the end
            of the layer. Default: None

    Inputs:
        A video feature of shape (N, D, H, W, C)
    Returns:
        Tensor of shape (N, D, H / 2, W / 2, 2 * C)
    """

    def __init__(self,
                 dim,
                 input_size,
                 depth,
                 num_heads,
                 window_size=(1, 7, 7),
                 mlp_ratio=4.,
                 qkv_bias=True,
                 qk_scale=None,
                 keep_prob=1.,
                 attn_keep_prob=1.,
                 droppath_keep_prob=1.,
                 norm_layer='layernorm',
                 downsample=None
                 ):
        super().__init__()
        self.window_size = window_size
        self.shift_size = tuple(i // 2 for i in window_size)
        self.depth = depth
        # build blocks
        self.blocks = nn.CellList([
            SwinTransformerBlock3D(
                dim=dim,
                num_heads=num_heads,
                input_size=input_size,
                window_size=self.window_size,
                shift_size=(0, 0, 0) if (i % 2 == 0) else self.shift_size,
                mlp_ratio=mlp_ratio,
                qkv_bias=qkv_bias,
                qk_scale=qk_scale,
                keep_prob=keep_prob,
                attn_keep_prob=attn_keep_prob,
                droppath_keep_prob=droppath_keep_prob[i] if isinstance(
                    droppath_keep_prob, list) else droppath_keep_prob,
                norm_layer=norm_layer
            )
            for i in range(depth)])
        self.downsample = downsample
        if self.downsample is not None:
            self.downsample = downsample(dim=dim, norm_layer=norm_layer)
        self.window_size, self.shift_size = limit_window_size(
            input_size, self.window_size, self.shift_size)
        self.attn_mask = compute_mask(
            input_size[0], input_size[1], input_size[2],
            self.window_size, self.shift_size)

    def construct(self, x):
        """Construct a basic stage layer for VideoSwinTransformer."""
        for blk in self.blocks:
            x = blk(x, self.attn_mask)
        if self.downsample is not None:
            x = self.downsample(x)
        return x


class PatchEmbed3D(nn.Cell):
    r"""
    Video to Patch Embedding.

    Args:
        patch_size (int): Patch token size. Default: (2,4,4).
        in_channels (int): Number of input video channels. Default: 3.
        embed_dim (int): Number of linear projection output channels.
            Default: 96.
        norm_layer (nn.Module, optional): Normalization layer. Default: None.

    Inputs:
        An original Video tensor in data format of 'NCDHW'.

    Returns:
        An embedded tensor in data format of 'NDHWC'.
    """

    def __init__(self, input_size=(16, 224, 224), patch_size=(2, 4, 4),
                 in_channels=3, embed_dim=96, norm_layer=None):
        super().__init__()
        self.patch_size = patch_size
        self.in_chans = in_channels
        self.embed_dim = embed_dim
        self.proj = nn.Conv3d(in_channels, embed_dim,
                              kernel_size=patch_size, stride=patch_size)
        if norm_layer == 'layernorm':
            if isinstance(self.embed_dim, int):
                self.embed_dim = (self.embed_dim,)
            self.norm = nn.LayerNorm(self.embed_dim)
        else:
            self.norm = Identity()
        self.output_size = [input_size[0] // patch_size[0],
                            input_size[1] // patch_size[1],
                            input_size[2] // patch_size[2]]

    def construct(self, x):
        """Construct Patch Embedding for 3D features."""
        # padding
        _, _, depth, height, width = x.shape
        pad_d1 = 0
        pad_b = 0
        pad_r = 0
        x_padded = []
        if width % self.patch_size[2] != 0:
            pad_r = self.patch_size[2] - width % self.patch_size[2]
        if height % self.patch_size[1] != 0:
            pad_b = self.patch_size[1] - height % self.patch_size[1]
        if depth % self.patch_size[0] != 0:
            pad_d1 = self.patch_size[0] - depth % self.patch_size[0]
        pad = nn.Pad(paddings=(
            (0, pad_d1),
            (0, pad_b),
            (0, pad_r),
            (0, 0)
        ))
        for i in range(x.shape[0]):
            x_b = x[i]
            x_b = pad(x_b)
            x_padded.append(x_b)
        x = ops.Stack(axis=0)(x_padded)

        x = self.proj(x)  # B C D Wh Ww
        if self.norm is not None:
            batch_size, channel_num, depth_w, height_w, width_w = x.shape
            x = x.reshape(batch_size, channel_num, -1).transpose(0, 2, 1)
            x = self.norm(x)
            x = x.view(-1, depth_w, height_w, width_w, channel_num)
        return x
