# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
""" Video Dataset Generator. """

import random
import imageio.v3 as iio


class DatasetGenerator:
    """ Dataset generator for getting video path and its corresponding label. """

    def __init__(self, path, label, seq=16, mode="part", suffix=".avi"):
        """
        Init Video Generator.
        Args:
           path(list): Video file path list.
           label(list): The label of each video,
           seq(int): The number of frames of the intercepted video.
           mode(str): Frame fetching method.
           suffix(str): Format of video file.
        """
        self.path = path
        self.label = label
        self.seq = seq
        self.mode = mode
        self.suffix = suffix

    def __getitem__(self, item):
        """Get the video and label for each item."""
        with open(self.path[item], 'rb')as rf:
            content = rf.read()
        video = iio.imread(content, index=None, format_hint=self.suffix)
        if self.mode == "part":
            start = random.randint(0, video.shape[0] - self.seq)
            video = video[start:start + self.seq]
        if self.mode == "discrete":
            index_list = random.sample([i for i in range(video.shape[0])], self.seq)
            index_list.sort()
            video = video[index_list]
        return video, self.label[item]

    def __len__(self):
        """Get the the size of dataset."""
        return len(self.path)
