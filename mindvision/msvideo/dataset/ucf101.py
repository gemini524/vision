# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
""" UCF101 dataset. TODO: uploade the dataset about 6.5G to huawei cloud obs gallery. """

import os
from mindvision.dataset.meta import ParseDataset
from mindvision.msvideo.dataset import transforms
from mindvision.msvideo.dataset.VideoDataset import VideoDataset
from mindvision.engine.class_factory import ClassFactory, ModuleType

__all__ = ["UCF101", "ParseUCF101"]


@ClassFactory.register(ModuleType.DATASET)
class UCF101(VideoDataset):
    """
     Args:
        path (string): Root directory of the Mnist dataset or inference image.
        split (str): The dataset split supports "train", "test" or "infer". Default: None.
        transform (callable, optional): A function transform that takes in a video. Default:None.
        target_transform (callable, optional): A function transform that takes in a label. Default: None.
        seq(int): The number of frames of captured video. Default: 16.
        seq_mode(str): The way of capture video frames,"part") or "discrete" fetch. Default: "part".
        batch_size (int): Batch size of dataset. Default:32.
        repeat_num (int): The repeat num of dataset. Default:1.
        shuffle (bool, optional): Whether or not to perform shuffle on the dataset. Default:None.
        num_parallel_workers (int): Number of subprocess used to fetch the dataset in parallel.Default: 1.
        num_shards (int, optional): Number of shards that the dataset will be divided into. Default: None.
        shard_id (int, optional): The shard ID within num_shards. Default: None.
        download (bool) : Whether to download the dataset. Default: False.

    Examples:
        >>> from mindvision.msvideo.dataset.ucf101 import UCF101
        >>> dataset = UCF101("./data/")
        >>> dataset = dataset.run()

    About UCF101 dataset:

        The UCF101 dataset consists of 13320 video in 101 classes.

        Here is the original UCF101 dataset structure.
        You can unzip the dataset files into the following directory structure and read them by MindSpore Vision's API.

        .
        |-ucf101                                     // contains 101 file folder
          |-- ApplyEyeMakeup                        // contains 145 videos
          |   |-- v_ApplyEyeMakeup_g01_c01.avi     // video file
          |   |-- v_ApplyEyeMakeup_g01_c02.avi     // video file
          |    ...
          |-- ApplyLipstick                         // contains 114 image files
          |   |-- v_ApplyLipstick_g01_c01.avi      // video file
          |   |-- v_ApplyLipstick_g01_c02.avi      // video file
          |    ...
          ...
    """

    def __init__(self,
                 path,
                 split=None,
                 transform=None,
                 target_transform=None,
                 seq=16,
                 seq_mode="part",
                 batch_size=16,
                 repeat_num=1,
                 shuffle=None,
                 num_parallel_workers=1,
                 num_shards=None,
                 shard_id=None,
                 download=False
                 ):
        load_data = ParseUCF101(path).parse_dataset
        super(UCF101, self).__init__(path=path,
                                     split=split,
                                     load_data=load_data,
                                     transform=transform,
                                     target_transform=target_transform,
                                     seq=seq,
                                     seq_mode=seq_mode,
                                     batch_size=batch_size,
                                     repeat_num=repeat_num,
                                     shuffle=shuffle,
                                     num_parallel_workers=num_parallel_workers,
                                     num_shards=num_shards,
                                     shard_id=shard_id,
                                     download=download)

    @property
    def index2label(self):
        """Get the mapping of indexes and labels."""
        mapping = []
        for cls in self.cls_list:
            mapping.append(cls)
        return mapping

    def download_dataset(self):
        """Download the UCF101 data if it doesn't exist already."""
        raise ValueError("UCF101 dataset download is not supported.")

    def default_transform(self):
        """Set the default transform for UCF101 dataset."""
        size = (224, 224)
        order = (3, 0, 1, 2)
        trans = [
            transforms.VideoResize(size),
            transforms.VideoReOrder(order),
        ]

        return trans


class ParseUCF101(ParseDataset):
    """
    Parse UCF101 dataset.
    """
    urlpath = "https://www.crcv.ucf.edu/data/UCF101.php#Results_on_UCF101"

    def parse_dataset(self):
        """Traverse the UCF101 dataset file to get the path and label."""
        parse_ucf101 = ParseUCF101(self.path)
        cls_name_list = os.listdir(parse_ucf101.path)
        video_label, video_path = [], []
        for i, cls_name in enumerate(cls_name_list):
            path_list = os.listdir(os.path.join(self.path, cls_name))
            for file_path in path_list:
                video_path.append(os.path.join(self.path, cls_name, file_path))
                video_label.append(i)
        return video_path, video_label

    def modify_struct(self):
        """If there is no category subdirectory in the folder, modify the file structure."""
        video_list = os.listdir(self.path)
        for video in video_list:
            cls_name = video.split("_")[1]
            input_file = os.path.join(self.path, video)
            output_file = os.path.join(self.path, cls_name)
            if not os.path.exists(output_file):
                os.mkdir(output_file)
            command = "mv {} {}".format(input_file, output_file)
            os.system(command)
