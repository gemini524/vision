package com.mindspore.vision.ui.bean;

public class CommonResultBean {
    private final String title;
    private final String content;
    private final float score;
    private final String position;

    public CommonResultBean(Builder builder) {
        this.title = builder.title;
        this.content = builder.content;
        this.score = builder.score;
        this.position = builder.position;
    }

    public static class Builder {
        private String title;
        private String content;
        private float score;
        private String position;

        public CommonResultBean build(){
            return new CommonResultBean(this);
        }

        public Builder setTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder setContent(String content) {
            this.content = content;
            return this;
        }

        public Builder setScore(float score) {
            this.score = score;
            return this;
        }

        public Builder setPosition(String position) {
            this.position = position;
            return this;
        }
    }


    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public float getScore() {
        return score;
    }

    public String getPosition() {
        return position;
    }
}
