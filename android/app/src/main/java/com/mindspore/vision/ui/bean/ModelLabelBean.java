package com.mindspore.vision.ui.bean;

import java.io.Serializable;
import java.util.List;

public class ModelLabelBean implements Serializable {

    private String title;
    private String file;
    private List<String> label;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public List<String> getLabel() {
        return label;
    }

    public void setLabel(List<String> label) {
        this.label = label;
    }
}
