package com.mindspore.vision.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.mindspore.vision.R;
import com.mindspore.vision.ui.adapter.ResultChildAdapter;
import com.mindspore.vision.ui.bean.CommonResultBean;

import java.util.ArrayList;
import java.util.List;

public class ResultChildFragment extends Fragment {

    private static final String ARG_SHOW_OSITION = "param1";
    private boolean isShowPosition;

    private RecyclerView recyclerView;
    private TextView positionText;
    private final List<CommonResultBean> dataList = new ArrayList<>();
    private ResultChildAdapter adapter;

    public static ResultChildFragment newInstance(boolean isShowPosition) {
        ResultChildFragment fragment = new ResultChildFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_SHOW_OSITION, isShowPosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isShowPosition = getArguments().getBoolean(ARG_SHOW_OSITION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_result_child_list, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.list);
        positionText = (TextView) view.findViewById(R.id.title_middle);
        if (isShowPosition){
            positionText.setVisibility(View.VISIBLE);
        }else {
            positionText.setVisibility(View.INVISIBLE);
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new ResultChildAdapter(getContext(), dataList);
        recyclerView.setAdapter(adapter);
    }

    public void setDataList(List<CommonResultBean> dataList) {
        this.dataList.clear();
        this.dataList.addAll(dataList);
        adapter.notifyDataSetChanged();
    }
}