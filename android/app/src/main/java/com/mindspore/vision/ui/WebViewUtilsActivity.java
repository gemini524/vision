/**
 * Copyright 2022 Huawei Technologies Co., Ltd
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mindspore.vision.ui;

import android.annotation.SuppressLint;
import android.os.Build;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;

import com.mindspore.vision.R;
import com.mindspore.vision.base.BaseActivity;
import com.mindspore.vision.view.AppTitleView;


public class WebViewUtilsActivity extends BaseActivity {

    private WebView mWebView;
    private ProgressBar progressBar;
    private static final String WEB_URL ="https://www.baidu.com";
    private final int ProgressBar = 100;

    @Override
    protected void init() {
        initView();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_webview_layout;
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initView() {
        progressBar = findViewById(R.id.progress);
        AppTitleView appTitle = findViewById(R.id.mWebView_toolbar);
        mWebView = findViewById(R.id.mWebView);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {

                if (newProgress == ProgressBar) {
                    progressBar.setVisibility(View.GONE);
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.setProgress(newProgress);
                }
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mWebView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }
        appTitle.setTitleText("推理与部署");
        mWebView.loadUrl(WEB_URL);
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        mWebView.removeAllViews();
        mWebView.destroy();
    }
}