package com.mindspore.vision.ui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mindspore.vision.R;
import com.mindspore.vision.ui.bean.CommonResultBean;

import java.util.List;

public class ResultChildAdapter extends RecyclerView.Adapter<ResultChildAdapter.ViewHolder> {

    private final List<CommonResultBean> mValues;
    private final Context mContext;

    public ResultChildAdapter(Context mContext, List<CommonResultBean> items) {
        mValues = items;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(mContext)
                .inflate(R.layout.fragment_result_child, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mTextLeft.setText(mValues.get(position).getTitle());
        holder.mTextRight.setText(mValues.get(position).getContent());
        if (TextUtils.isEmpty(mValues.get(position).getPosition())) {
            holder.mTextMiddle.setVisibility(View.INVISIBLE);
        } else {
            holder.mTextMiddle.setVisibility(View.VISIBLE);
            holder.mTextMiddle.setText(mValues.get(position).getPosition());
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView mTextLeft;
        public final TextView mTextRight;
        public final TextView mTextMiddle;

        public ViewHolder(View itemView) {
            super(itemView);
            mTextLeft = itemView.findViewById(R.id.item_left);
            mTextRight = itemView.findViewById(R.id.item_right);
            mTextMiddle = itemView.findViewById(R.id.item_middle);
        }
    }
}