package com.mindspore.vision.search;

import android.os.Environment;

import java.io.File;

public class FileItem {
    private final static String storagePath = Environment.getExternalStorageDirectory().getPath() + File.separator;

    private final String name, path;
    private final File file;
    private boolean isChecked;

    public FileItem(File file) {
        this.file = file;
        this.name = file.getName();
        this.path = file.getPath().replace(storagePath, "");
        isChecked = false;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public File getFile() {
        return file;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }
}