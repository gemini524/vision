package com.mindspore.vision.utils;

import android.os.Environment;

import java.io.File;

public class StorageUtils {

    public static final String CRASH_PATH = Environment.getExternalStorageDirectory().getPath()
            + "/MindSpore/CrashLog/";

    public static final File ABSOLUTE_FILE = Environment.getExternalStorageDirectory().getAbsoluteFile();
}
