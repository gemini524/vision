package com.mindspore.vision.ui.help;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.mindspore.vision.R;
import com.mindspore.vision.view.MSTabEntity;
import com.mindspore.vision.view.TabEntity;

import java.util.ArrayList;
import java.util.List;

public class ResultTabHelper {

    private final Context mContext;
    private static TabLayout mTabLayout;
    private static List<String> tabList;

    public ResultTabHelper(Context mContext, TabLayout mTabLayout, List<String> tabList) {
        this.mContext = mContext;
        ResultTabHelper.mTabLayout = mTabLayout;
        ResultTabHelper.tabList = tabList;
    }

    public void initTabLayout(BaseOnTabSelectedListener listener) {
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getCustomView() != null) {
                    TextView tabText = tab.getCustomView().findViewById(R.id.txtModel);
                    tabText.setTextColor(mContext.getResources().getColor(R.color.main_tab_text_checked));
                }
                if (listener != null) {
                    listener.onTabSelected(tab);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                if (tab.getCustomView() != null) {
                    TextView tabText = tab.getCustomView().findViewById(R.id.txtModel);
                    tabText.setTextColor(mContext.getResources().getColor(R.color.main_tab_text_uncheck));
                }
                if (listener != null) {
                    listener.onTabUnselected(tab);
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        addTabView(mContext);
    }

    public static void addTabView(Context mContext) {
        mTabLayout.removeAllTabs();
        ArrayList<MSTabEntity> mTabEntities = getTabEntity();
        for (int i = 0; i < mTabEntities.size(); i++) {
            mTabLayout.addTab(mTabLayout.newTab().setCustomView(getTabView(mContext, mTabEntities.get(i))));
        }
    }

    private static ArrayList<MSTabEntity> getTabEntity() {
        ArrayList<MSTabEntity> mTabEntities = new ArrayList<>();
        for (int i = 0; i < tabList.size(); i++) {
            mTabEntities.add(new TabEntity(tabList.get(i)));
        }
        return mTabEntities;
    }


    private static View getTabView(Context context, MSTabEntity tabEntity) {
        View view = View.inflate(context, R.layout.item_camera_model, null);
        TextView tabText = view.findViewById(R.id.txtModel);
        tabText.setText(tabEntity.getMSTabTitle());
        tabText.setTextColor(context.getResources().getColor(R.color.main_tab_text_uncheck));
        return view;
    }


    public interface BaseOnTabSelectedListener {

        void onTabSelected(TabLayout.Tab tab);

        void onTabUnselected(TabLayout.Tab tab);
    }
}
