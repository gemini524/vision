package com.mindspore.vision.ui.help;

import com.google.gson.Gson;
import com.mindspore.vision.ui.bean.ModelLabelBean;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class ResultJsonHelper {

    public static ModelLabelBean gonsAnalyzeJSON(String filePath) {
        String jsonData = readFile(filePath);
        Gson gson = new Gson();
        return gson.fromJson(jsonData, ModelLabelBean.class);
    }

    private static String readFile(String fileName) {
        String result = null;
        try {
            InputStream inputStream =  new FileInputStream(fileName);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes);
            baos.write(bytes, 0,bytes.length);
            result = new String(baos.toByteArray());
            baos.close();
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

}
